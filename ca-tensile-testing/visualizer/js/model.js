/**
 * Grid Visualizer
 * @author Łukasz Rosół
 */

var visualizer = window.visualizer || {};

if (typeof window != 'undefined') {
  visualizer = window.visualizer || {};
} else {
  visualizer = {};
}

if (!visualizer.model) {
  visualizer.model = {};
}

visualizer.model = (function () {
  var steps = [];
  function Grid(cells) {
    var cells = cells || [[]];
    var width = cells[0].length || 0;
    var height = cells.length || 0;
    return {
      forEach: function (cb) {
        for (var y in cells) {
          for (var x in cells[y]) {
            cb(cells[y][x], { x: x, y: y });
          }
        }
      },
      size: { x: width, y: height }
    }
  }
  return {
    parseData: function (result) {
      var temp = [];
      var cells = [];
      var lines = result.split('\n');
      for (var i = 0; i < lines.length; ++i) {
        var line = lines[i];
        if (line.indexOf("Step") > -1) {
          cells = [];
        } else {
          if (line.length > 1) {
            var row = line.trim().split("");
            cells.push(row);
          } else {
            temp.push(new Grid(cells));
          }
        }
      }
      steps = temp;
      console.log("Simulation data was loaded successfully.")
    },
    getGrid: function (i) {
      if (isNaN(Number(i))) console.error("Index of step is " + i + ". Return grid for step[0].");
      var grid = steps[i];
      if (!grid) throw new Error("Grid for step[" + i + "] not exists.");
      return grid;
    },
    getNumberOfSteps: function () {
      return steps.length;
    }
  };
})(jQuery);
