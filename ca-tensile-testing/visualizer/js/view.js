/**
 * Grid Visualizer
 * @author Łukasz Rosół
 */

var visualizer = window.visualizer || {};

if (typeof window != 'undefined') {
  visualizer = window.visualizer || {};
} else {
  visualizer = {};
}

if (!visualizer.view) {
  visualizer.view = {};
}

visualizer.view = (function ($) {
  var grid;
  var maxNumberOfCells = {
    width: 0,
    height: 0
  };
  function Color(id) {
    var colors = {
      A:        [51 , 204, 51 ], // green
      M:        [102, 140, 255], // blue
      I:        [255, 153, 51 ], // orange
      E:        [255, 255, 255], // white
      H:        [0  , 0  , 102], // blue
      default:  [221, 221, 221]  // grey
    };
    var rgb = colors[id] ? colors[id] : colors.default;
    return {
      getRGBString: function () {
        var r = rgb[0] < 16 ? "0" + rgb[0].toString(16) : rgb[0].toString(16);
        var g = rgb[1] < 16 ? "0" + rgb[1].toString(16) : rgb[1].toString(16);
        var b = rgb[2] < 16 ? "0" + rgb[2].toString(16) : rgb[2].toString(16);
        var retColor = "#" + r + g + b;
        return retColor;
      }
    }
  }
  var dom = {
    step: "#step",
    fileBrowser: "#file-browser",
    canvas: "#ca-space"
  };
  return {
    getFileBrowserId: function () {
      return dom.fileBrowser;
    },
    getStepId: function () {
      return dom.step;
    },
    getStepValue: function () {
      return parseInt($(dom.step).val());
    },
    getCanvasId: function () {
      return dom.canvas;
    },
    setStepRange: function (range) {
      $(dom.step).attr(range);
      $(dom.step).val(range.min);
    },
    setGrid: function (cells) {
      grid = cells;
      if (maxNumberOfCells.width < grid.size.x) maxNumberOfCells.width = grid.size.x;
      if (maxNumberOfCells.height < grid.size.y) maxNumberOfCells.height = grid.size.y;
    },
    setMaxNumberOfCells: function (size) {
      maxNumberOfCells = { width: size.x, height: size.y };
    },
    render: function () {
      var canvas = document.getElementById('ca-space');
      var context = canvas.getContext('2d');
      // resize canvas height
      canvas.height = canvas.width / maxNumberOfCells.width * maxNumberOfCells.height;
      // define scale factor and length of side for cell
      context.cell = {
        scale: {
          x: canvas.width / maxNumberOfCells.width,
          y: canvas.height / maxNumberOfCells.height
        },
        length: canvas.width / maxNumberOfCells.width
      }
      // define function drawing cells on canvas
      context.drawCell = function (pos, color) {
        this.beginPath();
        this.rect(pos.x * this.cell.scale.x, pos.y * this.cell.scale.y, this.cell.length, this.cell.length);
        this.fillStyle = color.getRGBString();
        this.fill();
      };
      // draw each cell
      grid.forEach(function (val, pos) {
        var color = new Color(val);
        context.drawCell(pos, color);
      });
    }
  };
})(jQuery);
