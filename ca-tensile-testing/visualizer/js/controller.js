/**
 * Grid Visualizer
 * @author Łukasz Rosół
 */

$(document).ready(function () {
  var view = visualizer.view;
  var model = visualizer.model;
  var reloadCanvas = function () {
    $(view.getCanvasId()).fadeOut(500, function () {
      try {
        view.setGrid(model.getGrid(view.getStepValue()));
        view.render();
      } catch (err) {
        alert("Simulation data is not loaded. " + err.message);
      } finally {
        $(view.getCanvasId()).fadeIn(1000);
      }
    });
  };
  $(view.getFileBrowserId()).change(function (event) {
    var input = event.target;
    var reader = new FileReader();
    reader.onload = function () {
      try {
        model.parseData(reader.result);
        view.setStepRange({ min: 0, max: model.getNumberOfSteps() - 1 });
        view.setMaxNumberOfCells(model.getGrid(model.getNumberOfSteps() - 1).size);
        reloadCanvas();
      } catch (err) {
        alert("Loading simulation data failed." + err.message);
      }
    };
    reader.readAsText(input.files[0]);
  });
  $(view.getStepId()).change(function () {
    var range = { min: 0, max: model.getNumberOfSteps() - 1 };
    var value = parseInt(this.value);
    if (value < range.min) this.value = range.min;
    if (value > range.max) this.value = range.max;
    reloadCanvas();
  });
});
