import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import space.cell.Al;
import space.cell.Cell;
import space.cell.Empty;
import space.cell.Hook;
import space.cell.Intermetalic;
import space.cell.Mg;
import space.cell.Type;

public class CellTest {

	@Test
	public void testEquals() {
		Cell c1 = new Empty(1);
		Cell c2 = new Empty(1);
		Cell c3 = new Empty(2);
		
		assertEquals(c1, c1);
		assertEquals(c1, c2);
		assertNotEquals(c1, c3);
	}
	
	@Test
	public void testGetType() {
		assertEquals(Type.Al, (new Al(0)).getType());
		assertEquals(Type.Empty, (new Empty(0)).getType());
		assertEquals(Type.Mg, (new Mg(0)).getType());
		assertEquals(Type.Intermetalic, (new Intermetalic(0)).getType());
		assertEquals(Type.Hook, (new Hook(0)).getType());
	}

}
