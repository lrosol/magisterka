import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import space.cell.Al;
import space.cell.Cell;
import space.cell.CellFactory;
import space.cell.Empty;
import space.cell.Hook;
import space.cell.Intermetalic;
import space.cell.Mg;
import space.cell.Type;

public class CellFactoryTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreateEachCellType() {
		assertEquals(Empty.class, CellFactory.cell(Type.Empty).getClass());
		assertEquals(Hook.class, CellFactory.cell(Type.Hook).getClass());
		assertEquals(Al.class, CellFactory.cell(Type.Al).getClass());
		assertEquals(Mg.class, CellFactory.cell(Type.Mg).getClass());
		assertEquals(Intermetalic.class, CellFactory.cell(Type.Intermetalic).getClass());
		
	}
	
	@Test
	public void testInitConcreateFactory() {
		CellFactory alFactory = new CellFactory(Type.Al);
		assertEquals(Al.class, alFactory.cell().getClass());
		assertNotEquals(alFactory.cell().getId(), alFactory.cell().getId());
	}
	
	@Test
	public void testEquals() {
		Cell c1 = CellFactory.cell(Type.Empty);
		Cell c2 = CellFactory.cell(Type.Empty);
		
		assertNotEquals(c1, c2);
	}
}
