import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.AbstractQueue;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import space.Grid;
import space.cell.Al;
import space.cell.Cell;
import space.cell.CellFactory;
import space.cell.Empty;
import space.cell.Type;
import space.position.Point;
import space.position.Position;
import space.position.PositionedCell;
import utils.Counter;

public class GridTest {
	private Grid grid;

	@Before
	public void setUp() throws Exception {
		this.grid = new Grid(10, 10);
		
	}

	@After
	public void tearDown() throws Exception {
		this.grid = null;
	}
	
	@Test
	public void testInitFromString() {
		String[] test = new String[] {
			"AAA\nIII\nMMM\nIII\n"
		};
		for (String data : test) {
			Grid grid = Grid.parse(data);
			assertEquals(new Point<Integer>(3, 4), grid.size());
			assertEquals(3, grid.numberOf(Type.Al));
			assertEquals(6, grid.numberOf(Type.Intermetalic));
			assertEquals(3, grid.numberOf(Type.Mg));
			assertEquals(data, grid.toString());
		}
	}
	
	@Test
	public void testForEach() {
		final Counter counter = new Counter();
		this.grid.forEach(cell -> counter.increase());
		
		int expected = 10 * 10;
		assertEquals(expected, counter.getValue());
	}
	
	@Test
	public void testNotNullableCells() {
		this.grid.forEach(cell -> assertNotNull(cell));
	}
	
	@Test
	public void testIteratorForOrderedPositions() {
		Point<Integer> size = new Point<Integer>(5,3);
		Grid grid = new Grid(size);
		
		Iterator<Position> locations = grid.orderedPositions();
		
		for (int j=0; j<3; ++j) {
			for(int i=0; i<5; ++i) {
				Position testPoint = locations.next();
				assertEquals(new Position(i, j), testPoint);
			}
		}
	}
	
	@Test
	public void testIteratorForRandomPositions() {
		Point<Integer> size = new Point<Integer>(5,3);
		Grid grid = new Grid(size);
		
		Iterator<Position> locations = grid.randomPositions();
		while(locations.hasNext()) {
			Position position = locations.next();
			grid.getCell(position).meet();
		}
		
		grid.forEach(cell -> assertTrue(cell.isMet()));
	}
	
	@Test
	public void testCustomInitialization() {
		Point<Integer> size = new Point<Integer>(5, 1);
		CellFactory alFactory = new CellFactory(Type.Al);
		AbstractQueue<Cell> cells = new LinkedBlockingQueue<Cell>();
		assertTrue(cells.add(alFactory.cell()));
		assertTrue(cells.add(alFactory.cell()));
		assertTrue(cells.add(alFactory.cell()));
		assertTrue(cells.add(alFactory.cell()));
		
		Grid grid = new Grid(size, cells);
		Iterator<Cell> abc = grid.iterator();
		assertEquals(Al.class, abc.next().getClass());
		assertEquals(Al.class, abc.next().getClass());
		assertEquals(Al.class, abc.next().getClass());
		assertEquals(Al.class, abc.next().getClass());
		assertEquals(Empty.class, abc.next().getClass());
		
	}
	
	@Test
	public void testCopyingGrid() {
		Point<Integer> size = new Point<Integer>(5, 1);
		Queue<Cell> cells = new ArrayDeque<Cell>();
		
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Empty));
		cells.add(CellFactory.cell(Type.Hook));
		cells.add(CellFactory.cell(Type.Intermetalic));
		cells.add(CellFactory.cell(Type.Mg));
		
		Grid grid = new Grid(size, cells);
		Grid copy = new Grid(grid);
		
		assertFalse(grid == copy);
		
		Collection<PositionedCell> originCells = grid.cells();
		Collection<PositionedCell> copyOfCells = copy.cells();
		
		assertEquals(grid, copy);
		originCells.forEach(cell -> assertTrue(copyOfCells.contains(cell)));
		
		Position position = new Position(0,0);
		assertEquals(grid.getCell(position), copy.getCell(position));
		
		Cell fake = copy.getCell(new Position(0,0));
		copy.set(new Position(0,0), fake);
		assertTrue(grid.getCell(position) != copy.getCell(position));
		assertEquals(grid.getCell(position), copy.getCell(position));
	}

	@Test
	public void testCloningOmitMeetProperty() {
		Point<Integer> size = new Point<Integer>(1,1);
		Queue<Cell> cells = new ArrayDeque<Cell>();
		cells.add(CellFactory.cell(Type.Al));
		
		Grid grid = new Grid(size, cells);
		grid.cells().forEach(cell -> cell.getCell().meet());
		Grid copy = new Grid(grid);
		
		assertTrue(grid.getCell(new Position(0,0)).isMet());
		assertFalse(copy.getCell(new Position(0,0)).isMet());
	}
	
	@Test
	public void testEquals() {
		Point<Integer> size = new Point<Integer>(2,1);
		
		Cell c1 = CellFactory.cell(Type.Empty);
		Cell c2 = CellFactory.cell(Type.Empty);

		Queue<Cell> cells1 = new ArrayDeque<Cell>();
		cells1.add(c1);
		cells1.add(c2);
		Grid g1 = new Grid(size, cells1);
		
		Queue<Cell> cells2 = new ArrayDeque<Cell>();
		cells2.add(c1);
		cells2.add(c2);
		Grid g2 = new Grid(size, cells2);
		
		Queue<Cell> cells3 = new ArrayDeque<Cell>();
		cells3.add(c2);
		cells3.add(c1);
		Grid g3 = new Grid(size, cells3);
		
		assertNotEquals(c1, c2);
		assertEquals(g1, g2);
		assertNotEquals(g1, g3);
	}
	
	@Test
	public void testToString() {
		Point<Integer> size = new Point<Integer>(3,2);
		Type[] types = new Type[] {
			Type.Al, Type.Hook, Type.Intermetalic,
			Type.Mg, Type.Empty, Type.Al
		};
		
		Grid grid = new Grid(size, types);
		
		String test = "AHI\nMEA\n";
		assertEquals(test, grid.toString());
	}

}
