import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import space.position.Point;

public class PointTest {

	@Test
	public void testEqauls() {
		Point<Integer> point = new Point<Integer>(2, 1);
		assertEquals(point.getX().getClass(), Integer.class);
		assertEquals(point.getY().getClass(), Integer.class);
		assertEquals(2, point.getX().intValue());
		assertEquals(1, point.getY().intValue());
	}
}
