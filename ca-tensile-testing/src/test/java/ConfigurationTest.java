import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import space.Grid;
import space.position.Point;

public class ConfigurationTest {

	@Test
	public void testParsingConfiguration() {
		String data = "4\nIIIII\nAAAAA\nAAAAA\nIIIII\n";
		Configuration config = Configuration.parse(Arrays.asList(data.split("\n")).stream());
		
		assertEquals(new Point<Integer>(5,4), config.getGrid().size());
		assertArrayEquals(new Integer[]{0, 3}, config.getIntermetalicRowsIndices());
		assertEquals(2, config.getMaxIntermetalicSize().intValue());
		assertEquals(Grid.parse(data.substring(2)).toString(), config.getGrid().toString());
	}
}
