import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayDeque;
import java.util.Queue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import space.Grid;
import space.Neighbourhood;
import space.cell.Al;
import space.cell.Cell;
import space.cell.CellFactory;
import space.cell.Intermetalic;
import space.cell.Mg;
import space.cell.Type;
import space.position.Point;
import space.position.Position;

public class NeighbourhoodTest {
	
	private Grid grid;

	@Before
	public void setUp() throws Exception {
		Point<Integer> size = new Point<Integer>(3,3);
		Queue<Cell> cells = new ArrayDeque<Cell>();
		
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Intermetalic));
		cells.add(CellFactory.cell(Type.Intermetalic));
		cells.add(CellFactory.cell(Type.Intermetalic));
		cells.add(CellFactory.cell(Type.Mg));
		cells.add(CellFactory.cell(Type.Mg));
		cells.add(CellFactory.cell(Type.Mg));
		
		grid = new Grid(size, cells);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNonPeriodicBehaviour() {
		Neighbourhood neighbourhood = Neighbourhood.create(new Position(1,1), grid::getCell);
		
		neighbourhood.forEach(cell -> assertNotNull(cell));
		
		assertNotNull(neighbourhood.leftUp());
		assertNotNull(neighbourhood.up());
		assertNotNull(neighbourhood.rightUp());
		
		assertNotNull(neighbourhood.left());
		assertNotNull(neighbourhood.center());
		assertNotNull(neighbourhood.right());
		
		assertNotNull(neighbourhood.leftDown());
		assertNotNull(neighbourhood.down());
		assertNotNull(neighbourhood.rightDown());
		
	    assertEquals(Al.class, neighbourhood.leftUp().getClass());
		assertEquals(Al.class, neighbourhood.up().getClass());
		assertEquals(Al.class, neighbourhood.rightUp().getClass());
		
		assertEquals(Intermetalic.class, neighbourhood.left().getClass());
		assertEquals(Intermetalic.class, neighbourhood.center().getClass());
		assertEquals(Intermetalic.class, neighbourhood.right().getClass());
	
		assertEquals(Mg.class, neighbourhood.leftDown().getClass());
		assertEquals(Mg.class, neighbourhood.down().getClass());
		assertEquals(Mg.class, neighbourhood.rightDown().getClass());
	}
	
	@Test
	public void testCellOnUpsideLeftCorner() {
		Neighbourhood neighbourhood = Neighbourhood.create(new Position(0,0), grid::getCell);
		
		assertNull(neighbourhood.leftUp());
		assertNull(neighbourhood.up());
		assertNull(neighbourhood.rightUp());
		
		assertNull(neighbourhood.left());
		assertNotNull(neighbourhood.center());
		assertNotNull(neighbourhood.right());
		
		assertNull(neighbourhood.leftDown());
		assertNotNull(neighbourhood.down());
		assertNotNull(neighbourhood.rightDown());
		
		assertEquals(Al.class, neighbourhood.center().getClass());
		assertEquals(Al.class, neighbourhood.right().getClass());

		assertEquals(Intermetalic.class, neighbourhood.down().getClass());
		assertEquals(Intermetalic.class, neighbourhood.rightDown().getClass());
	}

	@Test
	public void testCellOnDownsideRightCorner() {
		Neighbourhood neighbourhood = Neighbourhood.create(new Position(2,2), grid::getCell);
		
		assertNotNull(neighbourhood.leftUp());
		assertNotNull(neighbourhood.up());
		assertNull(neighbourhood.rightUp());
		
		assertNotNull(neighbourhood.left());
		assertNotNull(neighbourhood.center());
		assertNull(neighbourhood.right());
		
		assertNull(neighbourhood.leftDown());
		assertNull(neighbourhood.down());
		assertNull(neighbourhood.rightDown());
		

		assertEquals(Intermetalic.class, neighbourhood.leftUp().getClass());
		assertEquals(Intermetalic.class, neighbourhood.up().getClass());
		
		assertEquals(Mg.class, neighbourhood.left().getClass());
		assertEquals(Mg.class, neighbourhood.center().getClass());
	}
}
