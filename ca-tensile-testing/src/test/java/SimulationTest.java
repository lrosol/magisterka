import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.junit.Test;

import space.Grid;
import space.cell.Cell;
import space.cell.CellFactory;
import space.cell.Type;
import space.position.Point;
import space.position.Position;
import utils.Counter;

public class SimulationTest {
	private int getLengthOfCrush(Grid grid, Integer rowIndex) {
		Counter counter = new Counter();
		boolean found = false;
		int y = rowIndex;
		for(int x=0; x<grid.size().getX(); ++x) {
			Cell c = grid.getCell(new Position(x, y));
			if (found) {
				if (c.getType() == Type.Empty) {
					counter.increase();
				} else {
					return counter.getValue();
				}
			} else {
				if (c.getType() == Type.Empty) {
					found = true;
					counter.increase();
				}
			}
		}
		return counter.getValue();
	}

	@Test
	public void testIsFinishedBeforeSimulation() {
		Point <Integer> size = new Point<Integer>(1,1);
		Queue<Cell> cells = new ArrayDeque<Cell>();
		cells.add(CellFactory.cell(Type.Empty));
		
		Grid grid = new Grid(size, cells);
		Simulation simulation = new Simulation(grid);
		
		assertFalse(simulation.isFinished());
		assertEquals(0, simulation.getNumberOfSteps());
		assertEquals(grid, simulation.getStep(simulation.getNumberOfSteps()));
		
	}

	@Test
	public void testOneStepSimulation() {
		Point<Integer> size = new Point<Integer>(5, 1);
		Queue<Cell> cells = new ArrayDeque<Cell>();
		
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Al));
		
		Grid grid = new Grid(size, cells);
		
		Simulation simulation = new Simulation(grid);
		Grid stepsGrid = null;
		stepsGrid = simulation.processStep(grid);
		
		
		assertTrue(simulation.isFinished());
		assertEquals(1, simulation.getNumberOfSteps());
		assertEquals(grid, simulation.getStep(simulation.getNumberOfSteps()-1));
		assertEquals(grid, simulation.getLastGrid());
		assertEquals(simulation.getLastGrid(), stepsGrid);
	}
	
	@Test
	public void testExtendGridEveryStep() {
		Point<Integer> size = new Point<Integer>(1, 9);
		Queue<Cell> cells = new ArrayDeque<Cell>();
		
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Al));
		cells.add(CellFactory.cell(Type.Intermetalic));
		cells.add(CellFactory.cell(Type.Intermetalic));
		cells.add(CellFactory.cell(Type.Intermetalic));
		cells.add(CellFactory.cell(Type.Mg));
		cells.add(CellFactory.cell(Type.Mg));
		cells.add(CellFactory.cell(Type.Mg));

		Grid grid = new Grid(size, cells);
		Simulation simulation = new Simulation(grid, new Point<Integer>(10,9));
		simulation.process();
		
		assertEquals(10, simulation.getNumberOfSteps());
	}
	
	@Test
	public void testCreatingIntermetalic() {
		Point<Integer> size = new Point<Integer>(2, 1);
		Point<Integer> maxSize = new Point<Integer>(3, 1);
		
		Type[] types = new Type[] {
				Type.Al, Type.Mg
		};

		Grid grid = new Grid(size, types);
		Simulation simulation = new Simulation(grid, maxSize, new Integer[]{3,4,5}, 50);
		simulation.process();
		
		Grid result = simulation.getLastGrid();
		
		assertEquals(Type.Intermetalic, result.getCell(new Position(0,0)).getType());
		assertEquals(Type.Intermetalic, result.getCell(new Position(1,0)).getType());
		assertEquals(Type.Mg, result.getCell(new Position(2,0)).getType());
		
	}
	
	@Test
	public void testRandomCrushes() {
		Point<Integer> size = new Point<Integer>(1, 9);
		Point<Integer> maxSize = new Point<Integer>(3, 9);
		
		Type[] types = new Type[] {
				Type.Hook,			Type.Hook,			Type.Hook,
				Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,
				Type.Hook,			Type.Hook,			Type.Hook
		};

		Grid grid = new Grid(size, types);
		Simulation simulation = new Simulation(grid, maxSize, new Integer[]{3,4,5}, 50);
		simulation.process();

		
		assertEquals(3, simulation.getNumberOfSteps());
		Counter coutner = new Counter();
		simulation.getStep(1).forEach(cell -> {
			if (cell.getType() == Type.Empty) {
				coutner.increase();
			}
		});
		assertEquals(3, coutner.getValue());
		
		coutner.reset();
		simulation.getStep(2).forEach(cell -> {
			if (cell.getType() == Type.Empty) {
				coutner.increase();
			}
		});
		assertEquals(6, coutner.getValue());
	}
	
	@Test
	public void testMovingMetalToEmptySpace() {
		Point<Integer> size = new Point<Integer>(3, 5);
		Point<Integer> maxSize = new Point<Integer>(4, 5);
		
		Type[] types = new Type[] {
				Type.Al, 	Type.Al,	Type.Al,
				Type.Hook,	Type.Empty,	Type.Hook,
				Type.Hook, 	Type.Empty, Type.Hook,
				Type.Hook, 	Type.Empty, Type.Hook,
				Type.Mg, 	Type.Mg, 	Type.Mg
		};
		
		Grid grid = new Grid(size, types);
		Simulation simulation = new Simulation(grid, maxSize);
		simulation.process();


		assertEquals(2, simulation.getNumberOfSteps());
		assertEquals(3, simulation.getStep(0).numberOf(Type.Empty));
		assertEquals(1, simulation.getStep(1).numberOf(Type.Empty));
		assertEquals(0, simulation.getStep(2).numberOf(Type.Empty));
	}
	
	@Test
	public void testBasicExpandCrushes() {
		Point<Integer> size = new Point<Integer>(6, 3);
		Point<Integer> maxSize = new Point<Integer>(7, 3);
		
		Type[] types = new Type[] {
				Type.Hook, 			Type.Hook,			Type.Hook,			Type.Hook,			Type.Hook,			Type.Hook,
				Type.Intermetalic,	Type.Intermetalic,	Type.Empty,			Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,
				Type.Hook, 			Type.Hook, 			Type.Hook, 			Type.Hook, 			Type.Hook, 			Type.Hook
		};
		
		Grid grid = new Grid(size, types);
		Simulation simulation = new Simulation(grid, maxSize, new Integer[] {1}, 50);
		simulation.process();


		assertEquals(2, simulation.getNumberOfSteps());
		assertEquals(1, simulation.getStep(0).numberOf(Type.Empty));
		assertEquals(2, simulation.getStep(1).numberOf(Type.Empty));
		assertEquals(Type.Empty, simulation.getStep(1).getCell(new Position(2,1)).getType());
		boolean isEmptyOnTheRight = Type.Empty == simulation.getStep(1).getCell(new Position(3,1)).getType();
		boolean isEmptyOnTheLeft = Type.Empty == simulation.getStep(1).getCell(new Position(1,1)).getType();
		assertTrue(isEmptyOnTheRight || isEmptyOnTheLeft);
		assertEquals(2, getLengthOfCrush(simulation.getStep(1), 1));
	}
	
	@Test
	public void testAdvancedExpandCrushes() {
		Point<Integer> size = new Point<Integer>(6, 7);
		Point<Integer> maxSize = new Point<Integer>(8, 7);
		
		Type[] types = new Type[] {
				Type.Hook, 			Type.Hook,			Type.Hook,			Type.Hook,			Type.Hook,			Type.Hook,
				Type.Intermetalic,	Type.Intermetalic,	Type.Empty,			Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,
				Type.Empty,			Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,
				Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,	Type.Empty,			Type.Intermetalic,
				Type.Intermetalic,	Type.Empty,			Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,
				Type.Intermetalic,	Type.Empty,			Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,
				Type.Hook, 			Type.Hook, 			Type.Hook, 			Type.Hook, 			Type.Hook, 			Type.Hook
		};
		
		Grid grid = new Grid(size, types);
		Simulation simulation = new Simulation(grid, maxSize, new Integer[] {1,2,3,4,5}, 50);
		simulation.process();

		assertEquals(3, simulation.getNumberOfSteps());
		assertEquals(5, simulation.getStep(0).numberOf(Type.Empty));
		assertEquals(10, simulation.getStep(1).numberOf(Type.Empty));
		assertEquals(15, simulation.getStep(2).numberOf(Type.Empty));
	}
	
	@Test
	public void testExpandingRCrushes() {
		Point<Integer> size = new Point<Integer>(6, 3);
		Point<Integer> maxSize = new Point<Integer>(10, 3);
		
		Type[] types = new Type[] {
				Type.Hook, 			Type.Hook,			Type.Hook,			Type.Hook,			Type.Hook,			Type.Hook,
				Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,	Type.Intermetalic,
				Type.Hook, 			Type.Hook, 			Type.Hook, 			Type.Hook, 			Type.Hook, 			Type.Hook
		};
		
		Grid grid = new Grid(size, types);
		Simulation simulation = new Simulation(grid, maxSize, new Integer[] {1}, 50);
		simulation.process();


		assertEquals(5, simulation.getNumberOfSteps());
		assertEquals(0, simulation.getStep(0).numberOf(Type.Empty));
		assertEquals(1, simulation.getStep(1).numberOf(Type.Empty));
		assertEquals(2, simulation.getStep(2).numberOf(Type.Empty));
		assertEquals(3, simulation.getStep(3).numberOf(Type.Empty));
		assertEquals(4, simulation.getStep(4).numberOf(Type.Empty));
	}
	
	@Test
	public void test60x15for60steps() {
		Point<Integer> size = new Point<Integer>(60, 15);
		Point<Integer> maxSize = new Point<Integer>(120, 15);
	
		
		Type[] def = new Type[]{Type.Al, Type.Intermetalic, Type.Mg};
		List<Type> t = new ArrayList<Type>();
		for (int i=0; i<3; ++i) {
			for (int j=0; j<60*5; ++j) {
				t.add(def[i]);
			}
		}
		
		Grid grid = new Grid(size, t.toArray(new Type[900]));
		Simulation simulation = new Simulation(grid, maxSize, new Integer[] {5,6,7,8,9}, 50);
		simulation.process();
	}
}
