public class SimulationException extends Exception {
	private static final long serialVersionUID = -6013114498141608102L;

	public SimulationException() {
		// TODO Auto-generated constructor stub
	}

	public SimulationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SimulationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SimulationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SimulationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
}
