import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import utils.Counter;

public class Result {
	private BufferedWriter out;
	Counter counter = new Counter();
	
	public Result(Path path) throws IOException {
		out =  Files.newBufferedWriter(path);
	}
	public void write(String text) throws IOException {
		out.write("Step ["+counter.getValue()+"]:\n");
		out.write(text);
		out.newLine();
		out.flush();
		counter.increase();
	}
	public void close() throws IOException {
		out.close();
	}
}
