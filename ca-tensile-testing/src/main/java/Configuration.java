import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import space.Grid;
import space.cell.Type;
import space.position.Point;
import utils.Counter;

public class Configuration {	
	private static final Configuration empty = new Configuration(new Grid(0,0), new Point<Integer>(0,0), new Integer[0], 1);

	private final Grid grid;
	private final Point<Integer> maxSize;
	private final Integer[] intermetalicRowsIndices;
	private final Integer maxIntermetalicSize;

	public static Configuration parse(final Stream<String> data) {
		Configuration config = null;
		try {
			List<String> lines = data.collect(Collectors.toList());
			Integer maxNumberOfSteps = Integer.parseInt(lines.remove(0));
			Grid grid = Grid.parse(lines.stream().collect(Collectors.joining("\n")));
			Point<Integer> maxSize = new Point<Integer>(grid.size().getX()+maxNumberOfSteps, grid.size().getY());
			Integer[] intermetalicRowsIndices = getRowsWithIntermetalic(grid);
			Integer maxIntermetalicSize = getMaxIntermetalicSize(grid);
			return new Configuration(grid, maxSize, intermetalicRowsIndices, maxIntermetalicSize);
		} catch(Exception e) {
			config = Configuration.empty;
		}
		return config;
	}
	
	private static Integer[] getRowsWithIntermetalic(final Grid grid) {
		 Set<Integer> rows = grid.cells().stream()
				 .filter(cell -> cell.getCell().getType() == Type.Intermetalic)
				 .map(cell -> cell.getPosition().getY())
				 .collect(Collectors.toSet());
		 
		 return rows.toArray(new Integer[rows.size()]);
	}

	private static Integer getMaxIntermetalicSize(final Grid grid) {
		Counter c = new Counter();
		grid.getPositions().stream()
			.filter(position -> {
				boolean a = position.getX() == 0;
				boolean b = grid.getCell(position).getType() == Type.Intermetalic;
				return a && b;
			})
			.forEach(position -> c.increase());
		return Integer.valueOf(c.getValue());
	}
	public Configuration(Grid grid, Point<Integer> maxSize, Integer[] intermetalicRowsIndices, Integer maxIntermetalicSize) {
		super();
		this.grid = grid;
		this.maxSize = maxSize;
		this.intermetalicRowsIndices = intermetalicRowsIndices;
		this.maxIntermetalicSize = maxIntermetalicSize;
	}

	public Grid getGrid() {
		return grid;
	}

	public Point<Integer> getMaxSize() {
		return maxSize;
	}

	public Integer[] getIntermetalicRowsIndices() {
		return intermetalicRowsIndices;
	}
	
	public Integer getMaxIntermetalicSize() {
		return maxIntermetalicSize;
	}
}
