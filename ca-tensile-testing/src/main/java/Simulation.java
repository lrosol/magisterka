import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Stream;

import space.Grid;
import space.Neighbourhood;
import space.cell.Cell;
import space.cell.CellFactory;
import space.cell.Transformation;
import space.cell.Type;
import space.position.Point;
import space.position.Position;
import space.position.PositionedCell;
import utils.Counter;

public class Simulation extends Thread {
	private List<Grid> steps;
	private Point<Integer> maxSize;
	private int maxIntermetalicSize = 0;
	private Map<Integer, Boolean> crushes;
	private TransformationRegistry transformations;

	private static final Path results = Paths.get("output");
	private static final Path target = results.resolve(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
	
	public static void main(String[] args) throws IOException, SimulationException, InterruptedException {
		createOutputDirectories();
		ExecutorService executor = Executors.newCachedThreadPool();
		System.out.println("Simulation started.");
		getFileStream(args).parallel().forEach(file -> executor.submit(() -> processFile(file)));
		awaitOneHourAndFinish(executor);
		System.out.println("Simulation finished.");
	}
	
	private static void createOutputDirectories() throws IOException {
		if(!Files.isDirectory(results)) Files.createDirectory(results);
		if(!Files.isDirectory(target)) Files.createDirectory(target);
		System.out.println("Output path: " + target.toAbsolutePath().toString());
	}
	private static Stream<File> getFileStream(String[] args) {
		return new HashSet<String>(Arrays.asList(args)).stream()
		.map(File::new)
		.filter(file -> {
			if(!file.exists()) {
				System.out.println("Can't read file from path: "+file.getAbsolutePath());
				return false;
			}
			return true;
		});
	}
	private static void processFile(File file) {
		try {
			System.out.println("Processing file "+file.getName()+" within thread "+currentThread().getId()+".");
			Iterator<Grid> steps = new Simulation(Configuration.parse(Files.lines(file.toPath()))).iterator();
			Result result = new Result(target.resolve(file.getName()));
			while(steps.hasNext()) { result.write(steps.next().toString()); }
			System.out.println("Finished simulation for file " + file.getName() + ".");
			result.close();
		} catch (IOException e) {
			System.out.println("Failed computing for file:" + file.getName() + ".");
		}
	}
	private static void awaitOneHourAndFinish(ExecutorService executor) throws InterruptedException {
		executor.shutdown();
		executor.awaitTermination(1, TimeUnit.HOURS);
		if(!executor.isTerminated()) executor.shutdownNow();
		executor.awaitTermination(5, TimeUnit.SECONDS);
	}
	
	private class TransformationRegistry {
		private Map<Transformation, Function<Position, Boolean>> transformations;
		
		public TransformationRegistry() {
			this.transformations = new HashMap<Transformation, Function<Position, Boolean>>() {
				private static final long serialVersionUID = 1L;

				@Override
				public Function<Position, Boolean> get(Object key) {
			        if(!containsKey(key)) {
			            return cell -> true;
			        }
			        return super.get(key);
			    }
			};
			this.transformations.put(new Transformation(Type.Intermetalic, Type.Empty), (position) -> {
				Boolean isAlreadyCrushed = crushes.get(position.getY());
				if (isAlreadyCrushed != null && isAlreadyCrushed == true) {
					return false;
				}
				if (Math.random() > 0.75) {
					return false;
				}
				crushes.put(position.getY(), true);
				return true;
			});
			this.transformations.put(new Transformation(Type.Mg, Type.Intermetalic), (position) -> {
				int abc = getDistanceToAl(position);
				double p = 1 - (Math.log(abc) / Math.log(maxIntermetalicSize));
				double r = Math.random();
				return r <= p;
			});
			this.transformations.put(new Transformation(Type.Al, Type.Intermetalic), (position) -> {
				int abc = getDistanceToMg(position);
				double p = 1 - (Math.log(abc) / Math.log(maxIntermetalicSize));
				double r = Math.random();
				return r <= p;
			});
		}
		public boolean verify(Transformation transformation, Position position) {
			return this.transformations.get(transformation).apply(position);
		}
	}
	
	public Simulation(Grid grid) {
		steps = new ArrayList<Grid>();
		steps.add(grid);
		this.maxSize = grid.size();
		this.crushes = new HashMap<Integer, Boolean>();
		this.transformations = new TransformationRegistry();
	}
	public Simulation(Grid grid, Point<Integer> maxSize) {
		this(grid);
		this.maxSize = maxSize;
	}
	
	public Simulation(Grid grid, Point<Integer> maxSize, Integer[] intermetalicRows, Integer maxIntermetalicSize) {
		this(grid, maxSize);
		if (maxIntermetalicSize != null) this.maxIntermetalicSize = maxIntermetalicSize;
		this.crushes = new HashMap<Integer, Boolean>();
		Arrays.asList(intermetalicRows).forEach(rowPosition -> this.crushes.put(rowPosition, false));
	}
	
	public Simulation(Configuration config) {
		this(config.getGrid(), config.getMaxSize(), config.getIntermetalicRowsIndices(), config.getMaxIntermetalicSize());
	}
	
	public boolean isFinished() {
		if (steps.size() < 2) {
			return false;
		}

		Grid current = steps.get(steps.size()-1);
		Grid previous = steps.get(steps.size()-2);
		
		if (current.size().equals(previous.size())) {
			return true;
		}
		
		for(PositionedCell cell : current.cells()) {
			if (!previous.cells().contains(cell)) {
				return false;
			}
		}
		return true;
	}
	
	public Grid processStep(Grid grid) {
		// create grid for results current simulation step
		boolean canExtend = grid.size().getX() < maxSize.getX();
		final Grid nextStep = new Grid(new Point<Integer>(grid.size().getX()+(canExtend?1:0), grid.size().getY()));

		// elongate grid
		if (canExtend) {
			int x = grid.size().getX();
			for (int y=0; y<grid.size().getY(); ++y) {
				Cell c = grid.getCell(new Position(x-1, y));
				nextStep.set(new Position(x,y), c.getType());
			}
		}
		
		// reset crushes status
		crushes.forEach((key, value)-> {
			crushes.put(key, false);
		});
		
		// iterator on each cell
		Iterator<Position> positions = grid.randomPositions();
		while(positions.hasNext()) {
			Position position = positions.next();
			Cell cell = grid.getCell(position);
				
			Neighbourhood neighbourood = Neighbourhood.create(position, grid::getCell);
			Type transformatedType = cell.process(neighbourood);
			Cell transformatedCell = transformatedType != null ? CellFactory.cell(transformatedType) : cell;

			if (transformations.verify(new Transformation(cell.getType(), transformatedType), position)) {
				nextStep.set(position, transformatedCell);
			} else {
				nextStep.set(position, cell);
			}
		}
		
		// random crushes
		List<Integer> missingIntermetlicInRows = new ArrayList<Integer>();
		crushes.forEach((row, crushed) -> {
			if (crushed == false) {
				Random rand = new Random();
				Position crushPosition = null;
				Cell cell = null;
				Counter c = new Counter();
				do {
					crushPosition = new Position(rand.nextInt(grid.size().getX()), row);
					cell =  grid.getCell(crushPosition);
					if(cell == null) {
						missingIntermetlicInRows.add(row);
						break;
					}
					c.increase();
					if(c.getValue() > 10000) {
						System.out.println("ERROR\nNot found intermetalic for row: " + row +"\n" + grid.toString());
						missingIntermetlicInRows.add(row);
						break;
					}
				} while(cell.getType() != Type.Intermetalic);

				nextStep.set(crushPosition, Type.Empty);
			}
		});
		missingIntermetlicInRows.forEach(crushes::remove);
		
		steps.add(nextStep);
		return nextStep;
	}
	
	public void process() {
		Iterator<Grid> iterator = iterator();
		while(iterator.hasNext()) {
			iterator.next();
		}
	}
	
	public Iterator<Grid> iterator() {
		return new Iterator<Grid>(){
			@Override
			public boolean hasNext() {
				return !isFinished();
			}
			@Override
			public Grid next() {
				System.out.println("Processing step: " + steps.size());
				Grid grid = steps.get(steps.size()-1);
				processStep(grid);
				return grid;
			}
		};
	}
	
	public Grid getLastGrid() {
		return steps.get(steps.size()-1);
	}
	
	public int getNumberOfSteps() {
		return steps.size()-1;
	}

	public Grid getStep(int numberOfStep) {
		if (numberOfStep > getNumberOfSteps()) {
			return null;
		}
		return steps.get(numberOfStep);
	}
	
	private Integer getDistanceToAl(Position position) {
		Counter c = new Counter();
		final Grid grid = steps.get(steps.size()-1);
		for (int y = position.getY(); y>0; --y) {
			Position candidate = new Position(position.getX(), y);
			if (grid.getCell(candidate).getType() == Type.Al) {
				break;
			}
			c.increase();
		}
		return Integer.valueOf(c.getValue());
	}
	
	private Integer getDistanceToMg(Position position) {
		Counter c = new Counter();
		final Grid grid = steps.get(steps.size()-1);
		for (int y = position.getY(); y<grid.size().getY(); ++y) {
			Position candidate = new Position(position.getX(), y);
			if (grid.getCell(candidate).getType() == Type.Mg) {
				break;
			}
			c.increase();
		}
		return Integer.valueOf(c.getValue());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i=0; i<steps.size(); ++i) {
			builder.append("Step ["+i+"]:\n");
			builder.append(steps.get(i).toString());
			builder.append('\n');
		}
		return builder.toString();
	}
}
