package utils;
public class Counter {
	private int counter;
	
	public Counter() {
		reset();
	}
	
	public void increase() {
		++counter;
	}
	
	public int getValue() {
		return counter;
	}
	
	public void reset() {
		counter = 0;
	}
}