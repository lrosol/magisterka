package space.cell;

public class CellFactory {
	private static final IdGenerator ids = new IdGenerator();
	
	public static Cell cell(Type type) {
		Cell cell = null;
		try {
			cell = type.getClassOfCell().getConstructor(int.class).newInstance(ids.getNextId());
		} catch (Exception e) {
			e.printStackTrace();
			cell = new Empty(ids.getNextId());
		}
		return cell;
	}
	
	private final Type type;
	
	public CellFactory(Type type) {
		this.type = type;
	}
	
	public Cell cell() {
		return CellFactory.cell(type);
	}
	
	private static class IdGenerator {
		private int lastId = 0;
		
		public int getNextId() {
			return lastId++;
		}
	}
}
