package space.cell;
import java.util.Arrays;

public class Transformation {
	private final Type[] types;
	
	public Transformation(Type t1, Type t2) {
		this.types = new Type[] { t1, t2 };
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(types);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transformation other = (Transformation) obj;
		if (!Arrays.equals(types, other.types))
			return false;
		return true;
	}
}
