package space.cell;

import space.Neighbourhood;

public abstract class Cell implements Cloneable, Rule {
	private static Type type = Type.Empty;
	private int id;
	private Boolean met = false;

	public Cell(int id) {
		this.id = id;
	}
	
	public Type getType() {
		return Cell.type;
	}
	
	public int getId() {
		return id;
	}

	public Boolean isMet() {
		return met;
	}

	public void meet() {
		met = true;
	}
	
	public Cell clone() {
		return CellFactory.cell(this.getType());
	}
	
	@Override
	public Type process(final Neighbourhood neighbourhood) {
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cell other = (Cell) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
