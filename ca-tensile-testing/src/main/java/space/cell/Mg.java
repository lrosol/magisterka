package space.cell;

import space.Neighbourhood;

public class Mg extends Cell {
	private static final Type type = Type.Mg;

	public Mg(int id) {
		super(id);
	}
	
	@Override
	public Type getType() {
		return Mg.type;
	}
	
	@Override
	public Mg clone() {
		return new Mg(this.getId());
	}

	@Override
	public Type process(final Neighbourhood neighbourhood) {
		if (neighbourhood.contains(Type.Al)) return Type.Intermetalic;
		if (neighbourhood.up() != null && neighbourhood.up().getType() == Type.Intermetalic) return Type.Intermetalic;
		return null;
	}

	@Override
	public String toString() { 
		return type.toString();
	}
}
