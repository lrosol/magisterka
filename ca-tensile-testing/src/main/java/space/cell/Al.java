package space.cell;

import space.Neighbourhood;

public class Al extends Cell implements Cloneable {
	private static final Type type = Type.Al;

	public Al(int id) {
		super(id);
	}

	@Override
	public Type getType() {
		return Al.type;
	}

	@Override
	public Al clone() {
		return new Al(this.getId());
	}
	
	@Override
	public Type process(Neighbourhood neighbourhood) {
		if (neighbourhood.contains(Type.Mg)) return Type.Intermetalic;
		if (neighbourhood.down() != null && neighbourhood.down().getType() == Type.Intermetalic) return Type.Intermetalic;
		return null;
	}
	
	@Override
	public String toString() {
		return type.toString();
	}
}
