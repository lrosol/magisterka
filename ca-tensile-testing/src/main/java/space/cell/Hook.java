package space.cell;

public class Hook extends Cell {
	private static Type type = Type.Hook;

	public Hook(int id) {
		super(id);
	}
	
	@Override
	public Type getType() {
		return Hook.type;
	}
	
	@Override
	public Hook clone() {
		return new Hook(this.getId());
	}

	@Override
	public String toString() {
		return type.toString();
	}
}
