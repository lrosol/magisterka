package space.cell;

public enum Type {
	Empty(Empty.class, 'E'), 
	Hook(Hook.class, 'H'), 
	Al(Al.class, 'A'), 
	Mg(Mg.class, 'M'), 
	Intermetalic(Intermetalic.class, 'I');
	
	private Class<? extends Cell> classOfCell;
	private char alias; 
	
	Type(Class<? extends Cell> c, char a) {
		classOfCell = c;
		alias = a;
	}
	
	public Class<? extends Cell> getClassOfCell() {
		return classOfCell;
	}

	public Cell getInstance() {
		return CellFactory.cell(this);
	}
	
	public static Type fromChar(char c) {
		for (Type type : Type.values()) {
			if (type.alias == c) {
				return type;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return String.valueOf(alias);
	}
}