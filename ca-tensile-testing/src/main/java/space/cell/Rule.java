package space.cell;

import space.Neighbourhood;

public interface Rule {
	public Type process(final Neighbourhood neighbourhood);
}
