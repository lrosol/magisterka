package space.cell;

import space.Neighbourhood;

public class Intermetalic extends Cell {
	private static final Type type = Type.Intermetalic;

	public Intermetalic(int id) {
		super(id);
	}
	
	@Override
	public Type getType() {
		return Intermetalic.type;
	}
	
	@Override
	public Intermetalic clone() {
		return new Intermetalic(this.getId());
	}
	
	@Override
	public Type process(final Neighbourhood neighbourhood) {
		return neighbourhood.contains(Type.Empty) ? Type.Empty : null;
	}

	@Override
	public String toString() {
		return type.toString();
	}
}
