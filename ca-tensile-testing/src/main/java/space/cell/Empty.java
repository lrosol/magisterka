package space.cell;

import space.Neighbourhood;

public class Empty extends Cell {
	private static final Type type = Type.Empty;

	public Empty(int id) {
		super(id);
	}
	
	@Override
	public Type getType() {
		return Empty.type;
	}
	
	@Override
	public Empty clone() {
		return new Empty(this.getId());
	}
	
	@Override
	public Type process(final Neighbourhood neighbourhood) {
		boolean containsAl = neighbourhood.contains(Type.Al);
		boolean containsMg = neighbourhood.contains(Type.Mg);
		if (containsAl && containsMg) {
			int numberOfAl = neighbourhood.numberOf(Type.Al);
			int numberOfMg = neighbourhood.numberOf(Type.Mg);
			
			return numberOfAl > numberOfMg ? Type.Al : Type.Mg;
		}
		if (containsAl) {
			return Type.Al;
		}
		if (containsMg) {
			return Type.Mg;
		}
		return null;
	}

	@Override
	public String toString() {
		return type.toString();
	}
}
