package space;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import space.cell.Cell;
import space.cell.CellFactory;
import space.cell.Type;
import space.position.Point;
import space.position.Position;
import space.position.PositionedCell;
import utils.Counter;

public class Grid {
	private int height;
	private int width;
	private Cell[][] cells; 
	
	public Grid(int width, int height) {
		this.width = width;
		this.height = height;
		this.cells = new Cell[width][height];
		
		setAll(Type.Empty);
	}
	
	public static Grid parse(String data) {
		String[] rows = data.split("\n");
		
		int width = rows[0].length();
		int height = rows.length;
		
		Queue<Type> collection = new ArrayDeque<Type>();
		for (String row : rows) {
			for (char letter : row.toCharArray()) {
				Type type = Type.fromChar(letter);
				if (type != null) collection.add(type);
			}
		}
		
		Point<Integer> size = new Point<Integer>(width, height);
		Type[] types = collection.toArray(new Type[width*height]);
		return new Grid(size, types);
	}

	public Grid(Grid grid) {
		this(grid.size(), grid.cells());
	}
	
	public Grid(Point<Integer> size) {
		this(size.getX(), size.getY());
	}
	
	public Grid(Point<Integer> size, Collection<PositionedCell> cells) {
		this.width = size.getX().intValue();
		this.height = size.getY().intValue();
		this.cells = new Cell[width][height];
		
		cells.forEach(this::set);
	}
	
	public Grid(Point<Integer> size, Queue<? extends Cell> cells) {
		this.width = size.getX().intValue();
		this.height = size.getY().intValue();
		this.cells = new Cell[width][height];
		
		Iterator<Position> locations = this.orderedPositions();
		while(locations.hasNext()) {
			Cell cell = cells.poll();
			set(locations.next(), cell);
		}
	}
	
	public Grid(final Point<Integer> size, final Type[] types) {
		this.width = size.getX().intValue();
		this.height = size.getY().intValue();
		this.cells = new Cell[width][height];
		
		Iterator<Position> positions = this.orderedPositions();
		Iterator<Cell> cells = Arrays.asList(types).stream()
				.map(CellFactory::cell)
				.collect(Collectors.toList())
				.iterator();
		while(positions.hasNext()) {
			Cell cell = cells.hasNext() ? cells.next() : CellFactory.cell(Type.Empty);
			set(positions.next(), cell);
		}
	}
	
	public Collection<PositionedCell> cells() {
		Collection<PositionedCell> cells = new ArrayDeque<PositionedCell>();
		for (int j=0; j<height; ++j) {
			for(int i=0; i<width; ++i) {
				cells.add(new PositionedCell(this.cells[i][j], new Position(i,j)));
			}
		}
		return cells;
	}
	
	public Point<Integer> size() {
		return new Point<Integer>(width, height);
	}
	
	public Iterator<Cell> iterator() {
		return new Iterator<Cell>() {
			private Iterator<Position> locations = Grid.this.orderedPositions();

			@Override
			public boolean hasNext() {
				return locations.hasNext();
			}

			@Override
			public Cell next() {
				return Grid.this.getCell(locations.next());
			}
		};
	}
	
	public Iterator<Position> orderedPositions() {
		return new Iterator<Position>() {
			private int counter = 0;

			@Override
			public boolean hasNext() {
				return counter < Grid.this.height * Grid.this.width;
			}

			@Override
			public Position next() {
				Integer x = counter % Grid.this.width;
				Integer y = counter / Grid.this.width;
				Position position = new Position(x, y);

				counter += 1;
				return position;
			}
		};
	}
	
	public Iterator<Position> randomPositions() {
		return new Iterator<Position>() {
			
			private int toMeet = Grid.this.height * Grid.this.width;
			private List<Position> positions = Grid.this.getPositions();
			private Random rand = new Random();
			
			@Override
			public boolean hasNext() {
				return toMeet > 0;
			}

			@Override
			public Position next() {
				return positions.remove(rand.nextInt(toMeet--));
			}
		};
	}
	
	public Iterator<Neighbourhood> neighbours() {
		return new Iterator<Neighbourhood>() {
			
			private Iterator<Position> positions = Grid.this.randomPositions();

			@Override
			public boolean hasNext() {
				return positions.hasNext();
			}

			@Override
			public Neighbourhood next() {
				return Neighbourhood.create(positions.next(), Grid.this::getCell);
			}
			
		};
	}
	
	public List<Position> getPositions() {
		List<Position> positions = new ArrayList<Position>();
		for (int i=0; i<this.width; ++i) {
			for(int j=0; j<this.height; ++j) {
				positions.add(new Position(i,j));
			}
		}
		return positions;
	}
	
	public void forEach(Consumer<Cell> consumer) {
		for (Cell[] column : cells) {
			for (Cell cell : column) {
				consumer.accept(cell);
			}
		}
	}
	
	public Cell getCell(Position position) {
		if (position.getX() < 0 || position.getX() >= width){
			return null;
		}
		
		if (position.getY() < 0 || position.getY() >= height) {
			return null;
		}
		
		return cells[position.getX()][position.getY()];
	}
	
	public void set(Position location, Cell cell) {
		if (cell == null) {
			cell = CellFactory.cell(Type.Empty);
		}
		set(new PositionedCell(cell, location));
	}
	
	public void set(Position location, Type cell) {
		set(new PositionedCell(cell.getInstance(), location));
	}
	
	public void set(PositionedCell cell) {
		if (cell != null) {
			Integer x = cell.getPosition().getX();
			Integer y = cell.getPosition().getY();
			if (0 <= x && x < width && 0 <= y && y < height) {
				cells[x][y] = (Cell) cell.getCell().clone();
			}
		}
	}

	private void setAll(Type cell) {
		for (int i=0; i<this.width; ++i) {
			for(int j=0; j<this.height; ++j) {
				cells[i][j] = cell.getInstance();
			}
		}
	}
	
	public int numberOf(Type type) {
		Counter counter = new Counter();
		forEach(cell -> {
			if(cell.getType() == type) {
				counter.increase();
			}
		});
		return counter.getValue();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(cells);
		result = prime * result + height;
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grid other = (Grid) obj;
		if (!Arrays.deepEquals(cells, other.cells))
			return false;
		if (height != other.height)
			return false;
		if (width != other.width)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int y=0; y<this.height; ++y) {
			for (int x=0; x<this.width; ++x) {
				builder.append(cells[x][y].toString());
			}
			builder.append('\n');
		}
		return builder.toString();
	}
}
