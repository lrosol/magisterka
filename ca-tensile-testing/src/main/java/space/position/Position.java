package space.position;

public class Position {
	private final Point<Integer> position;
	
	public Position(int x, int y) {
		this.position = new Point<Integer>(x, y);
	}
	
	public int getX() {
		return position.getX().intValue();
	}
	public int getY() {
		return position.getY().intValue();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Position [" + getX() + "," + getY() + "]";
	}
}
