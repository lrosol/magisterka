package space.position;

import space.cell.Cell;

public final class PositionedCell {
	private final Cell cell;
	private final Position position;
	
	public PositionedCell(Cell cell, Position position) {
		this.cell = cell;
		this.position = position;
	}
	
	public Position getPosition() {
		return position;
	}
	
	public Cell getCell() {
		return cell;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cell == null) ? 0 : cell.hashCode());
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PositionedCell other = (PositionedCell) obj;
		if (cell == null) {
			if (other.cell != null)
				return false;
		} else if (!cell.equals(other.cell))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		return true;
	}
	
	
}
