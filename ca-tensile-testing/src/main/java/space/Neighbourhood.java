package space;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import space.cell.Cell;
import space.cell.Type;
import space.position.Position;

public class Neighbourhood {
	private final Cell[] cells;
	
	private Neighbourhood(Collection<Cell> cells) {
		this.cells = cells.stream().toArray(Cell[]::new);
	}
	
	public static Neighbourhood create(Position center, Function<Position, Cell> cellProvider) {
		Collection<Cell> cells = new ArrayList<Cell>();
		forEachNeighbourPosition(center, position -> cells.add(cellProvider.apply(position)));
		return new Neighbourhood(cells);
	}
	
	private static void forEachNeighbourPosition(Position center, Consumer<Position> consumer) {
		consumer.accept(new Position(center.getX()-1, center.getY()-1));
		consumer.accept(new Position(center.getX()  , center.getY()-1));
		consumer.accept(new Position(center.getX()+1, center.getY()-1));
		consumer.accept(new Position(center.getX()-1, center.getY()  ));
		consumer.accept(new Position(center.getX()  , center.getY()  ));
		consumer.accept(new Position(center.getX()+1, center.getY()  ));
		consumer.accept(new Position(center.getX()-1, center.getY()+1));
		consumer.accept(new Position(center.getX()  , center.getY()+1));
		consumer.accept(new Position(center.getX()+1, center.getY()+1));	
	}
	
	public void forEach(Consumer<Cell> consumer) {
		for (Cell cell : cells) {
			if (cell != null) {
				consumer.accept(cell);
			}
		}
	}
	
	public int numberOf(Type type) {
		return Arrays.asList(cells).stream()
				.filter(cell -> {
					return cell != null && cell.getType() == type;
				})
				.collect(Collectors.toList())
				.size();
	}
	
	public boolean contains(Type type) {
		for(Cell cell : cells) {
			if (cell != null && cell.getType() == type) {
				return true;
			}
		}
		return false;
	}
	
	public Cell leftUp() {
		return cells[0];
	}
	
	public Cell up() {
		return cells[1];
	}
	
	public Cell rightUp() {
		return cells[2];
	}
	
	public Cell left() {
		return cells[3];
	}
	
	public Cell center() {
		return cells[4];
	}
	
	public Cell right() {
		return cells[5];
	}
	
	public Cell leftDown() {
		return cells[6];
	}
	
	public Cell down() {
		return cells[7];
	}
	
	public Cell rightDown() {
		return cells[8];
	}
}
