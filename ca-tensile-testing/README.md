# CA Tensile Testing

Project realized within MA thesis.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites

What things you need to install the software and how to install them

* [Java 8](https://www.java.com/pl/download/)
* [Maven](https://maven.apache.org/download.cgi)

All commands presentet in this document should be run from command line from ca-tensile-testing dir.

## Build

A step by step series of examples that tell you have to get a development env running

Run from command line:

```
mvn package
```

It causes some things:
* Download all dependencies
* Run unit tests
* Build ca-tensile-testing.jar in /target dir

## Running the tests

Command for run unit tests:
```
mvn test
```

## Running program

Program should be built first.
Go to /ca-tensile-tesing/target and run command below:

```
java -jar ca-tensile-testing.jar input-1.txt input-2.txt
```

## Authors

* **Łukasz Rosół**
